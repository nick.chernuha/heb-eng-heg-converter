﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using System.Windows.Forms;

namespace WordAddIn1
{
    public partial class Ribbon1
    {
        StringBuilder sb = new StringBuilder();
        Dictionary<string, string> myDict = new Dictionary<string, string>();
        Dictionary<string, string> myDictEH = new Dictionary<string, string>();
        Dictionary<string, string> myDictHE = new Dictionary<string, string>();
        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {


            string[] eng = { "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "'\'", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "'", "z", "x", "c", "v", "b", "n", "m", ",", ".", "/" };
            string[] heb = { "/", "'", "ק", "ר", "א", "ט", "ו", "ן", "ם", "פ", "]", "[", "'\'", "ש", "ד", "ג", "כ", "ע", "י", "ח", "ל", "ך", "ף", ",", "ז", "ס", "ב", "ה", "נ", "מ", "צ", "ת", "ץ", "." };
            for (int i = 0; i < eng.Length; i++)
            {
                myDictEH.Add(eng[i], heb[i]);
                myDictHE.Add(heb[i], eng[i]);
            }

        }
        static string textFromDoc = "";
        private void button1_Click(object sender, RibbonControlEventArgs e)
        {
            Text_Swap(0);
        }


        private void button2_Click(object sender, RibbonControlEventArgs e)
        {
            Text_Swap(1);
        }

        private void Text_Swap(int opt)
        {

            textFromDoc = "";
            textFromDoc = Globals.ThisAddIn.Application.Selection.Text;

            string letter = null;
            textFromDoc = textFromDoc.ToLower();
            if (opt == 0)
                for (int i = 0; i < textFromDoc.Length; i++)
                {
                    if (myDictEH.TryGetValue(textFromDoc[i].ToString(), out letter))
                    {
                        sb.Append(letter);
                    }
                    else
                    {
                        sb.Append(textFromDoc[i].ToString());

                    }
                }
            if (opt == 1)
                for (int i = 0; i < textFromDoc.Length; i++)
                {
                    if (myDictHE.TryGetValue(textFromDoc[i].ToString(), out letter))
                    {
                        sb.Append(letter);
                    }
                    else
                    {
                        sb.Append(textFromDoc[i].ToString());

                    }
                }

            Microsoft.Office.Interop.Word.Range rng = Globals.ThisAddIn.Application.ActiveDocument.Range(Globals.ThisAddIn.Application.Selection.Start, Globals.ThisAddIn.Application.Selection.End);
            rng.Delete();
            if (opt == 0)
                Globals.ThisAddIn.Application.Selection.Paragraphs.ReadingOrder = Microsoft.Office.Interop.Word.WdReadingOrder.wdReadingOrderRtl;
            if (opt == 1)
                Globals.ThisAddIn.Application.Selection.Paragraphs.ReadingOrder = Microsoft.Office.Interop.Word.WdReadingOrder.wdReadingOrderLtr;
            rng.InsertAfter(sb.ToString());
            sb.Length = 0;
        }

    }
}
