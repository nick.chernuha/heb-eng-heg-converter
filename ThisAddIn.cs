﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;
using System.Windows.Forms;

namespace WordAddIn1
{
    public partial class ThisAddIn
    {
        public bool BeginGroup { get; private set; }
        public int FaceId { get; private set; }

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            //costum menu item event manager

        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }
        private void Application_WorkbookDeactivate(Word.Document Doc)
        {
        }

        private void Application_WorkbookActivate(Word.Document Doc)
        {
            Office.CommandBar ContextMenu = Application.CommandBars["Cell"];
            ContextMenu.Controls.Add(Office.MsoControlType.msoControlButton, 3, 1);
            Office.CommandBarControl MySubMenu;
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
